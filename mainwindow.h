#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QMenu>
#include <QMenuBar>
#include <QSystemTrayIcon>
#include <QMessageBox>
#include <QDateTime>

#include "ui_mainwindow.h"

#include "httpserver.h"
#include "protohandler.h"
#include "mercury119f.h"
#include "frconfig.h"
#include "logger.h"

#include "baseconnector.h"
#include "connectorcom.h"
#include "connectorusb.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow( QWidget *parent = 0 );
    bool createAllObjects();
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QPlainTextEdit *textEdit;
    FRConfig *frconfig;
    HttpServer *server;
    Mercury119F *mercury119F;
    ProtoHandler *handler;
    BaseConnector *baseConn;    

    bool mercuryConnect();        
    void closeEvent(QCloseEvent *event)
    {
        event->ignore();
        if (QMessageBox::Yes == QMessageBox::question(this, "Выход?", "Вы точно хотите выйти?", QMessageBox::Yes|QMessageBox::No))
        {
            event->accept();
        }
    }

private slots:
    void addLogWindow( QString message );
    void openSettings();
    void configComplete();    
    void showHideWindow();

    void trayIconClicked(QSystemTrayIcon::ActivationReason reason)
    {
        if(reason == QSystemTrayIcon::DoubleClick )
            showHideWindow();
    }

public slots:    
};

#endif // MAINWINDOW_H
