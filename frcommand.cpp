#include "frcommand.h"

void FRCommand::addString( QString in, quint32 size )
{
    quint32 slen = in.length();

    if( slen > size )
        slen = size;

    for( quint32 i=0; i<slen; i++ )
        cmd.push_back( in.toLocal8Bit()[i] );

    quint32 fill_length = size - slen;

    for( quint32 i=0; i<fill_length; i++ )
        cmd.push_back( '\0' );
}

void FRCommand::addByteArray( QByteArray in, quint32 size )
{
    quint32 slen = in.length();

    if( slen > size )
        slen = size;

    for( quint32 i=0; i<slen; i++ )
        cmd.push_back( in[i] );

    quint32 fill_length = size - slen;

    for( quint32 i=0; i<fill_length; i++ )
        cmd.push_back( '\0' );
}


void FRCommand::addNumber( quint32 in, quint32 size )
{
    /*
    std::ostringstream oss;
    oss << std::dec << in;

    std::string res = oss.str();
*/
    QString res = QString::number( in );

    quint32 fill_length = size - res.length();

    for( qint32 i=0; i<res.length(); i++ )
        cmd.push_back( res.toStdString()[i] );

    for( quint32 i=0; i<fill_length; i++ )
        cmd.push_back( '\0' );
}

void FRCommand::addBinary( QByteArray in, quint32 size )
{
    /*
    std::ostringstream oss;

    for( auto i: in )
        oss << std::hex << std::uppercase << std::setw( 2 ) << std::setfill( '0' ) << static_cast<quint32>( i );

    std::string res = oss.str();
    */

    QString res = in.toHex();
    quint32 fill_length = size - in.size();

    for( qint32 i=0; i<res.length(); i++ )
        cmd.push_back( res.toStdString()[i] );

    for( quint32 i=0; i<fill_length; i++ )
        cmd.push_back( '\0' );
}


// add char as string
void FRCommand::addChar( unsigned char in )
{
    /*
    std::ostringstream oss;
    oss << std::hex << std::uppercase << std::setw( 2 ) << std::setfill( '0' ) << static_cast<quint32>( in );
    */    

    QByteArray tmp;
    tmp.push_back( in );

    QString oss = tmp.toHex().toUpper();

    cmd.push_back( oss.toStdString()[0] );
    cmd.push_back( oss.toStdString()[1] );
}

void FRCommand::print()
{
    qDebug() << "[" << cmd.size() << "]: " << cmd.toHex();
}


void FRCommand::addCRC()
{
    unsigned char q = 0;    

    for( qint32 i=1; i<cmd.size(); i++ )
        q += cmd.at(i);

    addChar( q );
}
