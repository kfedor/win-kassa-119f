#ifndef BASECONNECTOR_H
#define BASECONNECTOR_H

#include "frcommand119f.h"
#include <QByteArray>
#include <QThread>
#include "logger.h"

#define STX '\2'

class BaseConnector
{
public:
    virtual ~BaseConnector() {};
    virtual bool sendCmd( FRCommand119F in, quint32 timeout ) = 0;
    QByteArray getReplyVector() { return reply_vector; }

protected:
    QByteArray reply_vector;
};

#endif // BASECONNECTOR_H
