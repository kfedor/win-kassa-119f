#ifndef PROTOHANDLER_H
#define PROTOHANDLER_H

#include <QString>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QTextCodec>
#include <QException>
#include <QMutexLocker>
#include <QMutex>
#include <QDateTime>
#include <QDate>
#include <QTimer>
#include <QTcpSocket>
#include <QHostAddress>
#include <QTimeZone>

#include <math.h>

#include "mercury119f.h"
#include "operationdb.h"
#include "logger.h"

#define PROTO_ERR_NOERR 0
#define PROTO_ERR_EXEC 100            // Ошибка выполнения команды на ККТ
#define PROTO_ERR_NO_REQUEST 101      // Отсутствует поле request
#define PROTO_ERR_WRONGDATE 102       // Неправильная дата
#define PROTO_ERR_WRONGTIME 103       // Неправильное время
#define PROTO_ERR_CASHIER_EMPTY 104   // Пусто имя кассира
#define PROTO_ERR_CASHIER_EXCEED 105  // Имя кассира превышает допустимую длину
#define PROTO_ERR_NO_PRODUCT 106      // Отсутствует название продукта
#define PROTO_ERR_NO_AMOUNT 107       // Отсутствует сумма
#define PROTO_ERR_PRODUCT_EXCEED 108  // Название продукта превышает длину
#define PROTO_ERR_BAD_FISCAL_TYPE 109 // Неподдерживаемый тип фискального документа
#define PROTO_ERR_BAD_OPER 110        // Неподдерживаемый тип финансовой операции
#define PROTO_ERR_UNKNOWN_COMMAND 111 // Неизвестная команда

class ProtoException : public QException
{
public:
    explicit ProtoException( int id, const char* message ): msg_( message ), id_( id ) {}
    explicit ProtoException( int id, const QString &message ): msg_( message ), id_( id ) {}

    void raise() const { throw *this; }
    ProtoException *clone() const { return new ProtoException(*this); }

    virtual QString what()
    {
        return msg_;
    }

    virtual int getErrorCode()
    {
        return id_;
    }

private:
    QString msg_;
    int id_;
};

class ProtoHandler : public QObject
{
    Q_OBJECT

public:
    ProtoHandler( Mercury119F *m, QHostAddress os, quint32 op, quint32 ofd_int );
    virtual ~ProtoHandler();
    QString parseRequest( QString uri, QString request );

private:    
    QTcpSocket *socketOFD;
    QTimer *timer;
    QMutex mutex;
    OperationDB operDB;
    Mercury119F *mercury;

    QHostAddress ofd_server;
    quint32 ofd_port;
    QByteArray ofd_data;

    bool parseJson( QString jstring, QJsonObject * );

    QJsonObject parse_v2_print( QJsonObject );
    QJsonObject parse_v2_datetime( QJsonObject );
    QJsonObject parse_v2_openday( QJsonObject );
    QJsonObject parse_v2_closeday( QJsonObject );
    QJsonObject parse_v2_internal_report( QJsonObject );
    QJsonObject parse_v2_fiscal_document( QJsonObject );
    QJsonObject parse_v2_x_report( QJsonObject );

    QByteArray utf8_866( QString in );
    QVector<QByteArray> getStringArray( QJsonArray data );    

private slots:
    void timerTick();
    void OFDReply();
    void OFDConnected();
    void OFDError( QAbstractSocket::SocketError error );
};

#endif // PROTOHANDLER_H
