#include "operationdb.h"

extern Logger *logger;

OperationDB::OperationDB()
{    
}

bool OperationDB::newOperationDay()
{
    QJsonDocument root_doc = loadJson();

    if( root_doc.isNull() )
    {
        logger->addErrorLog("OperationDB: error fetching JSON root");
        //return false;
    }

    QJsonObject root_obj = root_doc.object();
    QJsonArray root_arr = root_obj["db"].toArray();

    int db_size = root_arr.size();

    if( db_size > 4 )
    {
        root_arr.removeFirst();
    }

    QJsonArray arr; // empty
    QJsonObject obj;
    obj["id"] = root_arr.last().toObject()["id"].toInt()+1;    
    obj["operations"] = arr;

    root_arr.append( obj );

    QJsonObject out;
    out["db"] = root_arr;

    QJsonDocument root_out = QJsonDocument( out );
    return saveJson( root_out );
}

bool OperationDB::addOperation( QString amount_type, QString type, QString cash, QString good_price, QString operation )
{
    QJsonDocument root_doc = loadJson();

    if( root_doc.isNull() )
    {
        logger->addErrorLog("OperationDB: error fetching JSON root");
        return false;
    }

    QJsonObject root_obj = root_doc.object();
    QJsonArray root_arr = root_obj["db"].toArray();

    if( root_arr.size() == 0 )
    {
        logger->addErrorLog("OperationDB: JSON root empty");
        return false;
    }

    // последний опер день
    QJsonObject last_obj = root_arr.last().toObject();

    // последний массив операций
    QJsonArray last_arr = last_obj["operations"].toArray();

    // Новый объект
    QJsonObject obj;
    obj["type"] = type;
    obj["amount_type"] = amount_type;
    obj["cash"] = cash;
    obj["price"] = good_price;
    obj["operation"] = operation;
    obj["datetime"] = QDateTime::currentDateTime().toString( TIME_FORMAT );

    // Добавили новые данные в старый массив
    last_arr.append( obj );

    // Заменили массив операций
    last_obj["operations"] = last_arr;

    // Заменили последний опер день
    root_arr[ root_arr.size()-1 ] = last_obj;

    QJsonObject out;
    out["db"] = root_arr;

    QJsonDocument root_out = QJsonDocument( out );
    return saveJson( root_out );
}

QByteArray OperationDB::getLastOperDayForPrint( QJsonArray &last_arr )
{
    QByteArray out;
    QJsonDocument root_doc = loadJson();

    if( root_doc.isNull() )
    {
        logger->addErrorLog("OperationDB: error fetching JSON root");
        return out;
    }

    QJsonObject root_obj = root_doc.object();
    QJsonArray root_arr = root_obj["db"].toArray();

    if( root_arr.size() == 0 )
    {
        logger->addErrorLog("OperationDB: JSON root empty");
        return out;
    }

    // последний опер день
    QJsonObject last_obj = root_arr.last().toObject();

    // последний массив операций
    last_arr = last_obj["operations"].toArray();

    double sale_amt = 0.0;
    double withdrawal_amt = 0.0;
    double sale_return_amt = 0.0;
    double withdrawal_return_amt = 0.0;

    double sale_amt_cash = 0.0;
    double sale_amt_card = 0.0;
    double sale_return_amt_cash = 0.0;
    double sale_return_amt_card = 0.0;

    quint16 sale_count = 0;
    quint16 withdrawal_count = 0;
    quint16 sale_return_count = 0;
    quint16 withdrawal_return_count = 0;

    foreach( const QJsonValue &value, last_arr )
    {        
        QJsonObject obj = value.toObject();        
        QString type = obj["type"].toString();
        QString amount_type = obj["amount_type"].toString();

        double money = obj["price"].toString().toDouble();

        if( type == S_SALE )
        {
            sale_amt += money;
            sale_count++;

            if( amount_type == S_CASH )
                sale_amt_cash += money;
            else if( amount_type == S_CARD )
                sale_amt_card += money;
        }
        else if( type == S_WITHDRAWAL )
        {
            withdrawal_amt += money;
            withdrawal_count++;            
        }
        else if( type == S_SALE_RETURN )
        {
            sale_return_amt += money;
            sale_return_count++;

            if( amount_type == S_CASH )
                sale_return_amt_cash += money;
            else if( amount_type == S_CARD )
                sale_return_amt_card += money;
        }
        else if( type == S_WITH_RETURN )
        {
            withdrawal_return_amt += money;
            withdrawal_return_count++;
        }
    }

    out += utf8_866( QString("СВОДНЫЙ ОТЧЕТ ") );
    out += 0x0A;
    out += utf8_866( QString("Текущая дата: ") );
    out += utf8_866( QDateTime::currentDateTime().toString( TIME_FORMAT ) );
    out += 0x0A;

    // print total amt         
    /*
    out += utf8_866( QString("Количество продаж: %1").arg( sale_count ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма продаж: %1").arg( sale_amt, 0, 'f', 2 ) );
    out += 0x0A;
    out += utf8_866( QString("Количество возвратов: %1").arg( sale_return_count ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма возвратов: %1").arg( sale_return_amt, 0, 'f', 2 ) );
    out += 0x0A;
    out += utf8_866( QString("Количество выдач: %1").arg( withdrawal_count ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма выдач: %1").arg( withdrawal_amt, 0, 'f', 2 ) );
    out += 0x0A;
    out += utf8_866( QString("Количество возвратов выдач: %1").arg( withdrawal_return_count ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма возвратов выдач: %1").arg( withdrawal_return_amt, 0, 'f', 2 ) );
    out += 0x0A;
    out += utf8_866( QString("Количество операций: %1").arg( last_arr.size() ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма операций: %1").arg( sale_amt + withdrawal_return_amt - withdrawal_amt - sale_return_amt, 0, 'f', 2 ) );
    out += 0x0A;
    */

    out += utf8_866( QString("Количество продаж: %1").arg( sale_count ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма продаж: %1").arg( sale_amt, 0, 'f', 2 ) );
    out += 0x0A;

    out += utf8_866( QString("Сумма нал продаж: %1").arg( sale_amt_cash, 0, 'f', 2 ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма безнал продаж: %1").arg( sale_amt_card, 0, 'f', 2 ) );
    out += 0x0A;

    out += utf8_866( QString("Количество возвратов: %1").arg( sale_return_count ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма возвратов: %1").arg( sale_return_amt, 0, 'f', 2 ) );
    out += 0x0A;

    out += utf8_866( QString("Сумма нал возвратов: %1").arg( sale_return_amt_cash, 0, 'f', 2 ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма безнал возвратов: %1").arg( sale_return_amt_card, 0, 'f', 2 ) );
    out += 0x0A;

    out += utf8_866( QString("Количество выплат: %1").arg( withdrawal_count ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма выплат: %1").arg( withdrawal_amt, 0, 'f', 2 ) );
    out += 0x0A;

    out += utf8_866( QString("Количество операций: %1").arg( last_arr.size() ) );
    out += 0x0A;
    out += utf8_866( QString("Сумма операций: %1").arg( sale_amt + withdrawal_return_amt - withdrawal_amt - sale_return_amt, 0, 'f', 2 ) );
    out += 0x0A;
    out += "---------------------";

    out += QByteArray( 5, 0x0A ); // new line

    out += 0x1B; // cut paper
    out += 0x64;
    out += 0x30;
    out += 0x0C; // print buffer

    return out;
}

QJsonDocument OperationDB::loadJson()
{
    QJsonDocument tmp;
    QFile jsonFile( OPERATION_FILE );

    if( jsonFile.open( QFile::ReadOnly ) == false )
        return tmp;

    tmp = QJsonDocument::fromJson( jsonFile.readAll() );
    jsonFile.close();

    return tmp;
}

bool OperationDB::saveJson( QJsonDocument root )
{
    QFile jsonFile( OPERATION_FILE );

    if( jsonFile.open( QFile::WriteOnly ) == false )
    {
        logger->addErrorLog("OperationDB: error opening operDB for writing");
        return false;
    }

    if( jsonFile.write( root.toJson() ) == -1 )
    {
        logger->addErrorLog("OperationDB: error writing to openDB");
        jsonFile.close();
        return false;
    }

    jsonFile.close();

    return true;
}

QByteArray OperationDB::utf8_866( QString in )
{
    QTextCodec *codec = QTextCodec::codecForName("IBM 866");
    QByteArray encodedString = codec->fromUnicode( in );

    return encodedString;
}
