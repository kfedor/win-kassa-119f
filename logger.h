#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QFile>
#include <QDateTime>
#include <QMutex>
#include <QMutexLocker>

class Logger
{
public:
    Logger();
    void addErrorLog( QString );

private:
     QMutex mutex;
};

#endif // LOGGER_H
