#ifndef CONNECTORCOM_H
#define CONNECTORCOM_H

#include "baseconnector.h"
#include <QSerialPort>
#include <QDebug>

class ConnectorCOM : public BaseConnector
{
public:
    ConnectorCOM();
    virtual ~ConnectorCOM()
    {
        serial.close();
    };

    bool sendCmd( FRCommand119F in, quint32 timeout );

    bool init( QString port );

private:
    QSerialPort serial;
    bool readReply_vector( quint32 timeout );
};

#endif // CONNECTORCOM_H
