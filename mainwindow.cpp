#include "mainwindow.h"

extern Logger *logger;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{    
    ui->setupUi(this);    

    server = Q_NULLPTR;
    handler = Q_NULLPTR;
    mercury119F = Q_NULLPTR;
    baseConn = Q_NULLPTR;
    frconfig = Q_NULLPTR;

    // setup UI
    setWindowTitle( "Win-Kassa 119F v." + QString( KASSA_VERSION ) );
    setWindowFlags(Qt::WindowTitleHint);

    QIcon icon( ":/icons/letter-k-icon.png" );
    setWindowIcon( icon );

    QVBoxLayout *lay = new QVBoxLayout;
    lay->setSizeConstraint(QLayout::SetNoConstraint);

    QMenu *menuFile = menuBar()->addMenu(tr("&Меню"));
    QAction *openClose = new QAction( "Свернуть/развернуть", this );
    QAction *quitAction = new QAction( "Выход", this );
    QAction *configAction = new QAction("Настройки", this );

    connect( openClose, SIGNAL( triggered() ), this, SLOT( showHideWindow() ) );
    connect( quitAction, SIGNAL( triggered() ), this, SLOT( close() ) );
    connect( configAction, SIGNAL( triggered() ), this, SLOT( openSettings() ) );

    menuFile->setEnabled( true );
    menuFile->addAction( openClose );
    menuFile->addAction( configAction );
    menuFile->addSeparator();
    menuFile->addAction( quitAction );

    textEdit = new QPlainTextEdit();
    textEdit->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
    textEdit->setReadOnly( true );
    textEdit->setMaximumBlockCount( 500 );

    lay->addWidget( textEdit );
    ui->centralWidget->setLayout( lay );

    // Set Tray Icon
    QSystemTrayIcon *trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon( icon );
    trayIcon->setContextMenu( menuFile );
    connect( trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayIconClicked(QSystemTrayIcon::ActivationReason)) );
    trayIcon->show();

    addLogWindow( QString("Начало работы: версия %1").arg( KASSA_VERSION ) );        
}

bool MainWindow::createAllObjects()
{
    bool configured = false;

    // Config
    frconfig = new FRConfig();
    connect( frconfig, SIGNAL( configComplete() ), this, SLOT( configComplete() ) );

    configured = mercuryConnect();

    // Protocol handler
    QHostAddress ofd_server = frconfig->getOFDServer();
    quint32 ofd_port = frconfig->getOFDPort();
    quint32 ofd_int = frconfig->getOFDInterval();

    if( mercury119F )
        handler = new ProtoHandler( mercury119F, ofd_server, ofd_port, ofd_int );
    else
        return true; // не выходить из приложения

    addLogWindow( QString( "Сервер ОФД: %1:%2" ).arg(ofd_server.toString()).arg(ofd_port) );    

    // HTTP Server
    server = new HttpServer( handler );
    connect( server, SIGNAL( message(QString) ), this, SLOT( addLogWindow(QString) ) );

    if( !configured )
        return true; // не выходить из приложения

    if( !server->start( frconfig->getHttpPort() ) )
    {
        this->show();

        if( QMessageBox::Ok == QMessageBox::question(this, "Ошибка запуска", "Возможно приложение уже запущено", QMessageBox::Ok ) )
            return false;
    }

    return true;
}

MainWindow::~MainWindow()
{    
    delete server;
    delete handler;
    delete mercury119F;
    delete baseConn;
    delete frconfig;
    delete ui;
}

void MainWindow::showHideWindow()
{
    if( this->isVisible() )
    {
        this->hide();
    }
    else
    {
        this->show();
    }
}

void MainWindow::openSettings()
{    
    addLogWindow("Открытие окна настроек устройств");
    frconfig->show();
}

bool MainWindow::mercuryConnect()
{
    bool configured = false;
    baseConn = Q_NULLPTR;

    if( frconfig->getInterface() == "USB" )
    {
        ConnectorUSB *tmp = new ConnectorUSB();

        if( tmp->init() )
            baseConn = std::move( tmp );
    }
    else if( frconfig->getInterface() == "COM" )
    {
        ConnectorCOM *tmp = new ConnectorCOM();

        if( tmp->init( frconfig->getComPort() ) )
            baseConn = std::move( tmp );
    }
    else
    {
        addLogWindow("Меркурий 119Ф - Соединение не настроено");
        return configured;
    }

    // Device
    mercury119F = new Mercury119F( this, baseConn );
    connect( mercury119F, SIGNAL( message(QString) ), this, SLOT( addLogWindow(QString) ) );

    if( baseConn && mercury119F->open() )
    {
        configured = true;
    }
    else
    {
        addLogWindow("Меркурий 119Ф - Ошибка соединения");
        delete mercury119F;
        delete baseConn;
        mercury119F = Q_NULLPTR;
        baseConn = Q_NULLPTR;
    }

    return configured;
}

void MainWindow::configComplete()
{
    addLogWindow("Закрытие окна настроек устройств");
    addLogWindow("ВНИМАНИЕ! Для того, чтобы новые настройки вступили в силу, необходимо перезапустить приложение");
}

void MainWindow::addLogWindow( QString message )
{
    QDateTime date = QDateTime::currentDateTime();

    QString text = QString( "[%1] %2" ).arg( date.toString( TIME_FORMAT ) ).arg( message );
    textEdit->appendPlainText( text );
    logger->addErrorLog( message );
}
