#ifndef CONNECTORUSB_H
#define CONNECTORUSB_H

#include "baseconnector.h"

#if defined(Q_OS_LINUX)
#include <usb.h>
#else
#include <libusb-0.1/lusb0_usb.h>
#endif

#define USB_TIMEOUT  3000
#define BULK_EP_IN   0x81
#define BULK_EP_OUT  0x02
#define VID 0x0483 // Vendor ID
#define PID 0xfff0 // Product ID
#define INTERFACE_A     0
#define INTERFACE_B     1

class ConnectorUSB : public BaseConnector
{
public:
    ConnectorUSB();
    virtual ~ConnectorUSB();

    bool sendCmd( FRCommand119F in, quint32 timeout );
    bool init();

private:
    usb_dev_handle *dev;
    usb_dev_handle *openUSBdev( void );

    bool controlMsgIn( quint16 *exp_length, quint32 timeout );
    bool controlMsgOut( quint16 size );
};

#endif // CONNECTORUSB_H
