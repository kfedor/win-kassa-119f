#include "frcommand119f.h"


void FRCommand119F::addRequisiteByte( quint16 req_id, char value )
{
    addInt16( req_id );
    addInt16( 1 );
    addChar( value );
}

void FRCommand119F::addRequisiteVLN( quint16 req_id, quint32 value )
{
    addInt16( req_id );

    if( value <= 0xFF ) // 0 - 255
    {
        addInt16( 1 );
        addChar( value );
    }
    else if( value > 0xFF && value <= 0xFFFF ) // 256-65535
    {
        addInt16( 2 );
        addChar( value );
        addChar( value >> 8 );
    }
    else if( value > 0xFFFF && value <= 0xFFFFFF ) // 65536-...
    {
        addInt16( 3 );
        addChar( value );
        addChar( value >> 8 );
        addChar( value >> 16 );
    }
    else if( value > 0xFFFFFF )
    {
        addInt16( 4 );
        addChar( value );
        addChar( value >> 8 );
        addChar( value >> 16 );
        addChar( value >> 24 );
    }
}

// 1 будет представлена как 03 E8 03 = 1000 и десятичная точка на 3 месте
void FRCommand119F::addRequisiteFVLN( quint16 req_id, quint16 value )
{
    addInt16( req_id );
    addInt16( 3 ); // Длина
    addChar( 3 );  // Точка десятичная в конец
    addInt16( value*1000 ); // Обман
}

void FRCommand119F::addRequisiteASCII( quint16 req_id, QByteArray data )
{
    addInt16( req_id );
    addInt16( data.size() );
    cmd.append( data );
}

void FRCommand119F::addInt16( quint16 in )
{    
    cmd.push_back( in );
    cmd.push_back( in >> 8 );    
}

void FRCommand119F::addInt32( quint32 in )
{
    cmd.push_back( in );
    cmd.push_back( in >> 8 );
    cmd.push_back( in >> 16 );
    cmd.push_back( in >> 24 );
}

void FRCommand119F::addChar( unsigned char in )
{
    cmd.push_back( in );
}

void FRCommand119F::addByteArray( QByteArray in )
{
    cmd.append( in );
}

void FRCommand119F::print()
{
    printf( "[%d]: ", cmd.size() );

    for( qint32 i=0; i<cmd.size(); i++)
        printf("%02X ", (unsigned char)cmd.at(i));

    printf("\n");
}

quint16 FRCommand119F::getCRCCcitt( QByteArray data )
{
    quint16 crc_ccitt_FFFF = 0xFFFF;

    for( qint32 i=1; i<data.size(); i++ )
        crc_ccitt_FFFF = updateCrcCcitt( crc_ccitt_FFFF, data.at(i) );

    return crc_ccitt_FFFF;
}

void FRCommand119F::addCRCCcitt()
{
   addInt16( getCRCCcitt(cmd) );
}

void FRCommand119F::initCrcCcittTab( void )
{
    for( int i=0; i<256; i++ )
    {
        quint16 crc = 0;
        quint16 c = ( ( quint16 ) i ) << 8;

        for( int j=0; j<8; j++ )
        {

            if( ( crc ^ c ) & 0x8000 )
                crc = ( crc << 1 ) ^ P_CCITT;
            else
                crc =   crc << 1;

            c = c << 1;
        }

        crc_tabccitt[i] = crc;
    }
}

quint16 FRCommand119F::updateCrcCcitt( quint16 crc, char c )
{
    quint16 tmp=0x0, short_c=0x0;

    short_c  = 0x00ff & ( quint16 ) c;

    tmp = ( crc >> 8 ) ^ short_c;
    crc = ( crc << 8 ) ^ crc_tabccitt[tmp];

    return crc;
}
