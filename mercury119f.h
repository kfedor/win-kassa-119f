#ifndef MERCURY119F_H
#define MERCURY119F_H

#include <QObject>
#include <QDebug>
#include <QTextCodec>

#include "frcommand119f.h"
#include "baseconnector.h"

#define TIMEOUT_5 5000
#define TIMEOUT_15 15000
#define TIMEOUT_60 60000
#define TIMEOUT_120 120000

#define KASSA_VERSION "2.25"

class Mercury119F : public QObject
{
    Q_OBJECT

public:
    explicit Mercury119F( QObject *parent = Q_NULLPTR, BaseConnector *bc = Q_NULLPTR );
    virtual ~Mercury119F();

    enum class amount_type
    {
        amount_cash = 1,
        amount_card = 2
    };

    enum class doc_attr
    {
        sale = 1,               // приход
        sale_return = 2,        // возврат прихода
        withdrawal = 3,         // расход
        withdrawal_return = 4   // возврат расхода
    };

    enum class doc_type
    {
        open_session = 2,
        receipt_bso = 4,
        close_session = 8,
        current_payment_state = 23,
        session_report = 92
    };

    enum class controls
    {
        separator = 0x00,
        stx = 0x02,
        etx = 0x03
    };

    enum class cmds
    {
        get_version = 0x45,        
        fiscal = 0x53,
        config = 0x57,
        read_fr_params = 0x5C,              
        run_test = 0x65,
        readofdmessage = 0xA1,
        writefrmessage = 0XA2
    };

    bool getVersion();
    bool registerCashier( QByteArray surname );
    bool ZReport( QByteArray surname );
    bool XReport();
    bool readFRParams( quint16 param );
    bool runTest();
    bool setDateTime( quint32 unixtime );
    bool readMessageForOFDServer();
    bool writeMessageFromOFDServer( QByteArray in );
    bool reportCurrentPaymentState();    
    bool sale( amount_type atype, quint32 cash, quint32 good_price, QByteArray good_name, QByteArray cashier_name, doc_attr document_attribute );

    bool writeToFR( QByteArray data );
    bool cashBoxControl() { return true; }
    QString getCmdResultString() { return cmdResultToString( cmd_result ); }
    quint16 getCmdResult() { return cmd_result; }
    QString getFrStatus() { return fr_status; }
    QByteArray getOFDMessage() { return ofd_message; }
    unsigned char getPrinterStatus() { return printer_status; }
    QString cmdResultToString( quint16 );

    bool open();

signals:
    void message( QString );

private:
    BaseConnector *connector;

    QByteArray ofd_message;
    QByteArray fr_status;
    quint16 cmd_result;
    unsigned char printer_status;
    bool parseReply( QByteArray, unsigned char );
    QString cp866_to_unicode( QByteArray in );
    quint16 int16fromChar( char *data, int size );
    quint32 int32fromChar( char *data );    
};

#endif // MERCURY119F_H
