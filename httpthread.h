#ifndef HTTPTHREAD_H
#define HTTPTHREAD_H

#include <QObject>
#include <QThread>
#include <QTcpSocket>
#include <QByteArray>

#include "protohandler.h"

class HttpThread : public QThread
{
    Q_OBJECT
public:
    explicit HttpThread( int socketDescriptor, ProtoHandler *phandler );
    void run() override;

signals:
    void error( QTcpSocket::SocketError socketError );
    void message( QString );

private:
    int socketDescriptor;
    ProtoHandler *phandler;

    void processConnection( QTcpSocket *socket );
    QString parseHttpRequest( QByteArray in );
};

#endif // HTTPTHREAD_H

