#include "logger.h"

Logger::Logger()
{

}

void Logger::addErrorLog( QString text )
{
    QDateTime dateTime = QDateTime::currentDateTimeUtc();
    QTime time = QTime::currentTime();

    QString dateStr = dateTime.toString( "yyyy-MM-dd" );
    QString fileName = "errorLog-" + dateStr + ".log";

    QMutexLocker locker(&mutex);

    QFile f( fileName );

    if( f.open( QIODevice::WriteOnly | QIODevice::Append ) )
    {        
        QByteArray data = QString( "[%1] " ).arg( time.toString() ).toLatin1();
        data.append( text.toUtf8() );
        data.append( 0x0D );
        data.append( 0x0A );
        f.write( data.data(), data.size() );
        f.close();
    }
}
