#include "mercury119f.h"

quint16 Mercury119F::int16fromChar( char *data, int size )
{
    return (quint16)( ((unsigned char)data[size-1]) << 8 | ((unsigned char)data[size-2]) );
}

quint32 Mercury119F::int32fromChar( char *data )
{
    union
    {
        unsigned int integer;
        unsigned char byte[4];
    } char2int;

    char2int.byte[0] = data[0];
    char2int.byte[1] = data[1];
    char2int.byte[2] = data[2];
    char2int.byte[3] = data[3];

    return char2int.integer;
}

QString Mercury119F::cp866_to_unicode( QByteArray in )
{
    QByteArray in_stripped = QByteArray( in, in.indexOf( '\0' ) );
    QTextCodec *codec = QTextCodec::codecForName("IBM 866");
    QString string = codec->toUnicode( in_stripped );

    return string;
}

void printHex( char *data, int size )
{
    printf( "[%d]: ", size );

    for( qint32 i=0; i<size; i++ )
        printf( "%02X ", (unsigned char)data[i] );

    printf("\n");
}

Mercury119F::Mercury119F( QObject *parent, BaseConnector *bc ) : QObject(parent), connector(bc)
{
}

bool Mercury119F::open()
{
    if( connector == Q_NULLPTR )
    {
        emit message("Соединение с кассой не настроено");
        return false;
    }

    if( !getVersion() )
    {
        emit message("Ошибка получения информации о кассе");
        return false;
    }    

    emit message( QString("Меркурий 119Ф успешно подключен") );

    return true;
}

Mercury119F::~Mercury119F()
{        
    emit message( QString("Меркурий 119Ф остановлен") );
}

bool Mercury119F::parseReply( QByteArray v_data, unsigned char cmd_code )
{
    // tmp
    char *data = v_data.data();
    quint32 size = v_data.size();

   // printf("reply: ");
    //printHex( data, size );

    // Check length
    quint16 reply_length = (quint16)( ((unsigned char)data[2]) << 8 | ((unsigned char)data[1]) ) + 3;

    if( reply_length != size )
    {
        emit message( QString("Меркурий 119Ф: неверная длина ответа(%1 vs %2)").arg(reply_length).arg(size) );
        return false;
    }

    // Check CRC
    quint16 reply_crc = (quint16)( ((unsigned char)data[size-1]) << 8 | ((unsigned char)data[size-2]) );
    FRCommand119F tmp;
    quint16 reply_crc_calc = tmp.getCRCCcitt( QByteArray( data, size-2 ) );

    if( reply_crc != reply_crc_calc )
    {
        emit message( QString("Меркурий 119Ф: не совпадает контрольная сумма") );
        return false;
    }

    //reply_vector = QByteArray( data, size );

    fr_status = QByteArray( data+4, 2 );
    cmd_result = int16fromChar( data+6, 2 ); //QByteArray( data+6, 2 );

    //qDebug() << "Cmd result: " << cmd_result;
    //qDebug() << "FR status: " << fr_status.toHex();

    QString err_string = cmdResultToString(cmd_result);
    if( err_string.length() > 0 )
        emit message( err_string );

    printer_status = data[8];
    //qDebug() << "printer status: " << printer_status;

    if( data[3] != cmd_code )
    {
        emit message( QString("Меркурий 119Ф: неправильный код ответа, коллизия команд") );
        return false;
    }

    return true;
}

bool Mercury119F::getVersion()
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 3 ); // length
    cmd.addChar( (unsigned char)cmds::get_version );
    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_5 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::get_version ) )
        return false;

    QString serial_number = reply_vector.mid( 9, 20 );
    QString model = cp866_to_unicode( reply_vector.mid( 29, 30 ) );
    QString maker = cp866_to_unicode( reply_vector.mid( 59, 30 ) );
    emit message( QString("Серийный номер %1").arg(serial_number) );
    emit message( QString("Модель %1").arg(model) );
    emit message( QString("Производитель %1").arg(maker) );

    return true;
}

bool Mercury119F::runTest()
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 5 ); // length
    cmd.addChar( (unsigned char)cmds::run_test );
    cmd.addInt16( 0 ); // single test
    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_5 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::run_test ) )
        return false;

    return true;
}

bool Mercury119F::readFRParams( quint16 param )
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 5 ); // length
    cmd.addChar( (unsigned char)cmds::read_fr_params );
    cmd.addInt16( param );
    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_5 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::read_fr_params ) )
        return false;

    return true;
}

bool Mercury119F::writeToFR( QByteArray data )
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 7+data.size() ); // length
    cmd.addChar( (unsigned char)cmds::writefrmessage );
    cmd.addInt16( 1 );             // 1 = произвольные данные для вывода на печать
    cmd.addInt16( data.length() ); // длина сообщения
    cmd.addByteArray( data );
    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_60 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::writefrmessage ) )
        return false;

    return true;
}

bool Mercury119F::XReport()
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 7 ); // length
    cmd.addChar( (unsigned char)cmds::fiscal );
    cmd.addInt16( (quint16)doc_type::session_report );
    cmd.addInt16( 0 );
    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_120 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::fiscal ) )
        return false;

    return true;
}

// 02 11 00 53 02 00 0a 00 fd 03 06 00 56 61 73 69 6c 79 7a 88
bool Mercury119F::registerCashier( QByteArray surname )
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 11+surname.size() ); // length
    cmd.addChar( (unsigned char)cmds::fiscal );
    cmd.addInt16( (quint16)doc_type::open_session ); // Регистрация кассира - открытие смены

    // реквизиты документа 2
    cmd.addInt16( 4+surname.size() ); // длина реквизита
    cmd.addInt16( 1021 );             //  номер реквизита Кассир 1021
    cmd.addInt16( surname.size() );   // Длина имени кассира
    cmd.addByteArray( surname );

    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_5 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::fiscal ) )
        return false;

    return true;
}

//02 15 00 53 08 00 0e 00 fd 03 0a 00 88 a2 a0 ad ae a2 20 88 2e 88 9a 76
bool Mercury119F::ZReport( QByteArray surname )
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 11+surname.size() ); // length
    cmd.addChar( (unsigned char)cmds::fiscal );
    cmd.addInt16( (quint16)doc_type::close_session ); // закрытие смены

    // реквизиты документа 2
    cmd.addInt16( 4+surname.size() ); // длина реквизита
    cmd.addInt16( 1021 );             //  номер реквизита Кассир 1021
    cmd.addInt16( surname.size() );   // Длина имени кассира
    cmd.addByteArray( surname );

    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_15 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::fiscal ) )
        return false;

    return true;
}

// 02 0f 00 57 0b 00 08 00 [18 44 ee 58 00 00 00 00] c1 e0
// UnixTime: 24 68 238 88 00 00 00 00
//02 08 00 57 00 80 00 00 0E C7 AB
bool Mercury119F::setDateTime( quint32 unixtime )
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 15 ); // length
    cmd.addChar( (unsigned char)cmds::config );
    cmd.addInt16( 11 ); // Дата Время
    cmd.addInt16( 8 );  // UnixTime 4 байт длина + какие то еще 4 ноля
    cmd.addInt32( unixtime ); // 4 байт
    cmd.addInt16( 0 ); // 2 ноля
    cmd.addInt16( 0 ); // 2 ноля
    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_5 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::config ) )
        return false;

    return true;
}

// 02 03 00 a1 07 30
//"02 25 01 a1 00 80 00 00 0e [06000000] [6c142659] [1301]
//
//2a08410a81a2000139393939303738393030303032393938f50014000000f5002571a502011009999907890000299800000650e9b5b1af000001000400cb0200c70011041000393939393037383930303030323939380d0414003030393939393030303130343236373020202020fa030c003030373730383530313538321004040006000000f40304006c142659350406002304629ba09c0e0404000200000018042800202020202020202020202020208e8e8e2080e1e2aee02092e0a5a9a4202020202020202020202020f10328002020202020a32e208caee1aaa2a020e3ab2e203136208fa0e0aaaea2a0ef20a42e32362020202020fd030d00566173696c79208fe3afaaa8ad8106f26a716216560000
//48b4" CRC

bool Mercury119F::writeMessageFromOFDServer( QByteArray data )
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 7+data.size() ); // length
    cmd.addChar( (unsigned char)cmds::writefrmessage );
    cmd.addInt16( 0 );             // 0 - сообщение от сервера ОФД
    cmd.addInt16( data.length() ); // длина сообщения
    cmd.addByteArray( data );
    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_5 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::writefrmessage ) )
        return false;

    return true;
}


bool Mercury119F::readMessageForOFDServer()
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 3 ); // length
    cmd.addChar( (unsigned char)cmds::readofdmessage );
    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_5 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::readofdmessage ) )
        return false;

    //qDebug() << "OFD Message: " << reply_vector.toHex();

    quint32 ofd_number = int32fromChar( reply_vector.data()+9 );
    // datetime << int32fromChar( reply_vector.data()+9+4 );
    quint16 ofd_msg_len = int16fromChar( reply_vector.data()+9+4+4, 2 );

    if( getCmdResult() == 0x000C )
    {
        return false;
    }

    emit message( QString("Отправка сообщения №%1 на ОФД Сервер").arg(ofd_number) );

    ofd_message = QByteArray( reply_vector.data()+9+4+4+2, ofd_msg_len );

    return true;
}

// Отчет о текущем состояни и расчетов
// 02 07 00 53 17 00 00 00 e0 59
bool Mercury119F::reportCurrentPaymentState()
{
    FRCommand119F cmd;

    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 7 ); // length
    cmd.addChar( (unsigned char)cmds::fiscal );
    cmd.addInt16( (quint16)doc_type::current_payment_state ); // Отчет о текущем состоянии расчетов
    cmd.addInt16( 0 );
    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_5 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::fiscal ) )
        return false;

    return true;
}

// Ставка
/*
02
52 00 // 7 байт + 75
53 // fiscal
04 00 // Кассовый чек
4b 00 // 75 длина всего ниже

23 04 // 1059 - реквизит товар
25 00 // 37 - длина?
06 04 // 1030 - наименование товара
10 00 // 16 - длина
91 e2 a0 a2 aa a0 20 ad a0 20 e4 e3 e2 a1 ae ab (Ставка на футбол)
37 04 // 1079 - Цена за единицу VLN
01 00 // 1 длина
64    // 100 значение
ff 03 // 1023 - количество FVLN
03 00 // длина 3
03 e8 03 // 1.000
85 03 // 901 - налоговая группа товара
01 00 // длина
03    // 3 группа НДС не облагается

fd 03 // 1021 - реквизит кассир
0a 00 // 10 длина
88 a2 a0 ad ae a2 20 88 2e 88 // "Иванов И.И"
1f 04 // 1055
01 00 // длина 1
01    // 1
1e 04 // 1054 - признак расчета
01 00
01    // 1 - Приход
07 04 // 1031 - форма расчета наличными VLN
01 00 // длина 1
64    // 100 - 1.00 руб
39 04 // 1081 - форма расчета электронными VLN
01 00
00    // 0 руб
21 a1 // CRC

// read fr params
02 05 00 5c c8 00 09 86 // CRC
*/
bool Mercury119F::sale( amount_type atype, quint32 cash, quint32 good_price, QByteArray good_name, QByteArray cashier_name, doc_attr document_attribute )
{
    FRCommand119F cmd;
    FRCommand119F reqGroup;
    FRCommand119F reqGroupGoods;

    // реквизит структура: товар
    reqGroup.addInt16( 1059 ); // Реквизит Товар

    // реквизиты товара
    reqGroupGoods.addRequisiteASCII( 1030, good_name );                // Наименование товара
    reqGroupGoods.addRequisiteVLN( 1079, good_price );                 // Цена за единицу с копейками
    reqGroupGoods.addRequisiteFVLN( 1023, 1 );                         // Количество
    reqGroupGoods.addRequisiteByte( 901, 3 );                          // Налоговая группа

    reqGroup.addInt16( reqGroupGoods.size() );                         // размер всех реквизитов группы Товар
    reqGroup.addByteArray( reqGroupGoods.get() );                      // реквизиты

    // Другие реквизиты
    reqGroup.addRequisiteASCII( 1021, cashier_name );                  // Кассир
    reqGroup.addRequisiteByte( 1055, 1 );                              // Это система налогообложения по таблице 3.1.5 из документации. То что в документации нумеруется реквизитом 1062.

    reqGroup.addRequisiteByte( 1054, (quint16)document_attribute );    // Признак расчета 1 - приход

    if( atype == amount_type::amount_cash )
    {
        reqGroup.addRequisiteVLN( 1031, cash );                            // расчет наличными (100 = 1.00р)
        reqGroup.addRequisiteVLN( 1081, 0 );                               // расчет электронными
    }
    else
    {
        reqGroup.addRequisiteVLN( 1031, 0 );                            // расчет наличными (100 = 1.00р)
        reqGroup.addRequisiteVLN( 1081, cash );                               // расчет электронными
    }

    // Fill CMD
    cmd.addChar( (unsigned char)controls::stx );
    cmd.addInt16( 7+reqGroup.size() ); // length
    cmd.addChar( (unsigned char)cmds::fiscal );

    cmd.addInt16( (quint16)doc_type::receipt_bso ); // Кассовый чек БСО
    cmd.addInt16( reqGroup.size() );       // Длина группы реквизитов
    cmd.addByteArray( reqGroup.get() );    // Все данные реквизитов

    cmd.addCRCCcitt();

    if( !connector->sendCmd( cmd, TIMEOUT_5 ) )
        return false;

    QByteArray reply_vector = connector->getReplyVector();

    if( !parseReply( reply_vector, (unsigned char)cmds::fiscal ) )
        return false;

    return true;
}

QString Mercury119F::cmdResultToString( quint16 err )
{
    QString ret;

    switch( err )
    {
    /*
    case 0x0000:
        ret = "0000H Успешное выполнение команды.";
        break;*/

    case 0x0001:
        ret = "0001H Ошибка в данных энергонезависимой памяти.";
        break;

    case 0x0002:
        ret = "0002H Ошибка чтения flash памяти.";
        break;

    case 0x0003:
        ret = "0003H Ошибка записи flash памяти.";
        break;

    case 0x0004:
        ret = "0004H Оформление документа прервано по окончанию времени ожидания готовности принтера.";
        break;

    case 0x0005:
        ret = "0005H Вывод прерван по окончанию времени ожидания готовности дисплея.";
        break;

    case 0x0006:
        ret = "0006H Текущее состояние ККТ не позволяет выполнить операцию.";
        break;

    case 0x0007:
        ret = "0007H Выполняется тестирование оборудования ККТ.";
        break;

    case 0x0008:
        ret = "0008H Дата меньше последней даты, зарегистрированной в ФН. Необходимо выполнить команду программирования даты.";
        break;

    case 0x0009:
        ret = "0009H Расхождение текущей даты и даты последнего документа в ФН больше запрограммированного значения. Необходимо выполнить команду программирования даты.";
        break;

    case 0x000A:
        ret = "000AH Операция прервана пользователем.";
        break;

    case 0x000B:
        ret = "000BH ККТ не зарегистрирована.";
        break;
/*
    case 0x000C:
        ret = "000CH Нет сообщений для сервера ОФД.";
        break;*/

    case 0x000D:
        ret = "000DH Переполнение приёмного буфера.";
        break;

    case 0x000E:
        ret = "000EH Неверная контрольная сумма команды.";
        break;

    case 0x000F:
        ret = "000FH Нет такой команды.";
        break;

    case 0x0010:
        ret = "0010H Неверный формат команды.";
        break;

    case 0x0011:
        ret = "0011H Неверный формат поля команды.";
        break;

    case 0x0012:
        ret = "0012H Обязательное поле команды имеет нулевую длину.";
        break;

    case 0x0013:
        ret = "0013H Превышена длина поля команды.";
        break;

    case 0x0014:
        ret = "0014H Значение поля команды вне допустимого диапазона.";
        break;

    case 0x0015:
        ret = "0015H Дублирование реквизитов документа.";
        break;

    case 0x0050:
        ret = "0050H Переполнение счётчика итоговой суммы в чеке при добавлении.";
        break;

    case 0x0051:
        ret = "0051H Переполнение счётчика итоговой суммы в чеке при вычитании.";
        break;

    case 0x0052:
        ret = "0052H Переполнение счётчика итоговой суммы в чеке при вычислении надбавки.";
        break;

    case 0x0053:
        ret = "0053H Переполнение счётчика итоговой суммы в чеке при вычислении скидки.";
        break;

    case 0x0054:
        ret = "0054H Переполнение счётчика итоговой суммы в чеке при начислении налога.";
        break;

    case 0x0060:
        ret = "0060H Переполнение счётчика суммы по налоговой группе в чеке.";
        break;

    case 0x0061:
        ret = "0061H Переполнение счётчика суммы по налоговой группе в чеке при вычислении надбавки.";
        break;

    case 0x0062:
        ret = "0062H Переполнение счётчика суммы по налоговой группе в чеке при вычислении скидки.";
        break;

    case 0x0070:
        ret = "0070H Уплаченная сумма не равна итоговой сумме чека.";
        break;

    case 0x0100:
        ret = "0100H Ошибка связи с ФН.";
        break;

    case 0x0101:
        ret = "0101H Неизвестная команда, неверный формат посылки или неизвестные параметры ФН.";
        break;

    case 0x0102:
        ret = "0102H Неверное состояние ФН.";
        break;

    case 0x0103:
        ret = "0103H Ошибка ФН.";
        break;

    case 0x0104:
        ret = "0104H Ошибка криптографического сопроцессора ФН.";
        break;

    case 0x0105:
        ret = "0105H Закончен срок эксплуатации ФН.";
        break;

    case 0x0106:
        ret = "0106H Архив ФН переполнен.";
        break;

    case 0x0107:
        ret = "0107H Неверные дата и/или время ФН.";
        break;

    case 0x0108:
        ret = "0108H Нет запрошенных данных ФН.";
        break;

    case 0x0109:
        ret = "0109H Некорректное значение параметров команды ФН.";
        break;

    case 0x0110:
        ret = "0110H Превышение размеров TLV данных ФН.";
        break;

    case 0x0111:
        ret = "0111H Нет транспортного соединения ФН.";
        break;

    case 0x0112:
        ret = "0112H Исчерпан ресурс криптографического сопроцессора ФН.";
        break;

    case 0x0114:
        ret = "0114H Исчерпан ресурс хранения ФН.";
        break;

    case 0x0115:
        ret = "0115H Исчерпан ресурс ожидания передачи сообщения ФН.";
        break;

    case 0x0116:
        ret = "0116H Продолжительность смены ФН более 24 часов.";
        break;

    case 0x0117:
        ret = "0117H Неверная разница во времени между двумя операциями ФН.";
        break;

    case 0x0120:
        ret = "0120H Сообщение от ОФД не может быть принято ФН.";
        break;
/*
    default:
        ret = "Кошмар! Такой ошибки нет в документации!!11 И это просто невозможно.";
        break;*/
    };

    return ret;
}
