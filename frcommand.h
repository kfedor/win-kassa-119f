#ifndef FRCOMMAND_H
#define FRCOMMAND_H

#include <QString>
#include <QByteArray>
#include <QDebug>

#define MASKADD(a,b,mask) (((a & mask)+(b & mask)) & mask)

class FRCommand
{
public:
    void addString( QString, quint32 size );
    void addByteArray( QByteArray, quint32 size );
    void addNumber( quint32, quint32 size );
    void addBinary( QByteArray, quint32 size );
    void addChar( unsigned char in );
    void addByte( unsigned char in ) { cmd.push_back( in ); }
    void addCRC();      // For MS-K
    void print();
    void clear() { cmd.clear(); }

    QByteArray get() { return cmd; }

private:
    QByteArray cmd;
};

#endif // FRCOMMAND_H
