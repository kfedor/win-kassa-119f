#include "connectorusb.h"

extern Logger *logger;

ConnectorUSB::ConnectorUSB()
{
    dev = Q_NULLPTR;
}

ConnectorUSB::~ConnectorUSB()
{
    if( dev )
    {
        usb_release_interface( dev, 0 );
        usb_close( dev );
    }
}

bool ConnectorUSB::init()
{
    usb_init(); /* initialize the library */
    usb_find_busses(); /* find all busses */
    usb_find_devices(); /* find all connected devices */

    if( !(dev = openUSBdev()) )
    {
        //emit message( QString("Ошибка открытия Меркурий 119Ф") );
        logger->addErrorLog("Ошибка открытия Меркурий 119Ф USB");
        return false;
    }

    if( usb_set_configuration( dev, 1 ) < 0 )
    {
        logger->addErrorLog( "Ошибка настройки USB интерфейса 1" );
        return false;
    }

    if( usb_claim_interface( dev, 0 ) < 0 )
    {
        logger->addErrorLog( "Ошибка настройки USB интерфейса 2" );
        return false;
    }

    return true;
}

usb_dev_handle *ConnectorUSB::openUSBdev( void )
{
    struct usb_bus *bus;
    struct usb_device *dev;

    for( bus = usb_get_busses(); bus; bus = bus->next )
    {
        for( dev = bus->devices; dev; dev = dev->next )
        {
            if( dev->descriptor.idVendor == VID && dev->descriptor.idProduct == PID )
            {
                return usb_open(dev);
            }
        }
    }

    return NULL;
}

bool ConnectorUSB::controlMsgOut( quint16 size )
{
    char tmp[2];

    tmp[0] = size;
    tmp[1] = size >> 8;
    int ret = usb_control_msg( dev, USB_TYPE_VENDOR | USB_RECIP_INTERFACE | USB_ENDPOINT_OUT, 0xFD, 0, 0, tmp, sizeof(tmp), 0 );

    logger->addErrorLog( QString("USB Control MSG OUT: [%1]").arg( ret ) );

    //printf("control out: ");
    //printHex( tmp, 2 );

    return true;
}

// Проверка на приходящие сообщения
bool ConnectorUSB::controlMsgIn( quint16 *exp_length, quint32 timeout )
{
    for( int i=0; i<(timeout/200); i++ )
    {
        char tmp[2] = {0};
        int ret = usb_control_msg( dev, USB_TYPE_VENDOR | USB_RECIP_INTERFACE | USB_ENDPOINT_IN, 0xFE, 0, 0, tmp, sizeof(tmp), 0 );

        logger->addErrorLog( QString("USB Control MSG IN: [%1] %2 %3").arg( ret ).arg( tmp[0] ).arg( tmp[1] ) );

       // printf("control in: ");
      //  printHex( tmp, 2 );

        if( tmp[0] != 0 )
        {
            *exp_length = (quint16)( ((unsigned char)tmp[1]) << 8 | ((unsigned char)tmp[0]) );
          //  printf("in data ready\n");
            return true;
        }

#if defined(Q_OS_LINUX)
        usleep(200000); // Linux 200 millisec
#else
        Sleep(uint(200));  //WINDOWS only
#endif
    }

    return false;
}

bool ConnectorUSB::sendCmd( FRCommand119F in, quint32 timeout )
{
    QByteArray request = in.get();    

    logger->addErrorLog( QString("USB Request[%1]: [%2]").arg( request.size() ).arg( QString(request.toHex()) ) );

    int ret = usb_bulk_write( dev, BULK_EP_OUT, request.data(), request.size(), 0 );

    if( ret < 0 )
    {
        logger->addErrorLog( "Меркурий 119Ф: Ошибка записи" );
        return false;
    }

    controlMsgOut( request.size() ); // типа вроде записали

    // ========================================================================================================================
    // READ
    // ========================================================================================================================

    quint16 expected_length = 0;
    char reply[512] = {0};

    QByteArray tmp_vector;
    reply_vector.clear();

    if( controlMsgIn( &expected_length, timeout ) )  // начинаем ждать приход
    {        
        logger->addErrorLog( QString("Ожидаемая длина [%1]").arg( expected_length ) );

        for( int i=0; i<100; i++ )
        {            
            ret = usb_bulk_read( dev, BULK_EP_IN, reply, sizeof(reply), 0 );

            //logger->addErrorLog( QString("Блок данных [%1]").arg( ret ) );

            if( ret < 0 )
            {               
                logger->addErrorLog( "Меркурий 119Ф: Ошибка чтения" );
                return false;
            }

            // Проверим длину
            tmp_vector.append( reply, ret );
            //reply_vector.append( reply, ret );

            if( tmp_vector.size() >= expected_length )
            {
                //printf( "Good packet(length %d)\n", ret );
                reply_vector = tmp_vector.mid( 0, expected_length );
                controlMsgOut( 0 );
                break;
            }
            else
            {
               // printf( "Incomplete packet(length %d)\n", ret );
            }
        }
    }

    logger->addErrorLog( QString( "USB Reply[%1]: %2" ).arg(reply_vector.size()).arg( QString(reply_vector.toHex()) ) );

    if( expected_length > 0 && reply_vector.size() == expected_length )
    {
        logger->addErrorLog("USB успешный прием данных");
        return true;
    }

    logger->addErrorLog("USB ошибочный прием данных");
    return false;
}
