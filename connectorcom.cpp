#include "connectorcom.h"

extern Logger *logger;

ConnectorCOM::ConnectorCOM()
{

}

bool ConnectorCOM::sendCmd( FRCommand119F in, quint32 timeout )
{
    QByteArray cmd = in.get();

    logger->addErrorLog( QString("COM Request: [%1]").arg( QString(cmd.toHex()) ) );
    reply_vector.clear();

    if( serial.write(cmd) == -1 )
    {        
        logger->addErrorLog("Меркурий МС-К: Ошибка записи в порт");
        return false;
    }

    if( !serial.waitForBytesWritten(2000) )
    {
        logger->addErrorLog("Меркурий МС-К: Таймаут записи в порт");
        return false;
    }

    return readReply_vector( timeout );
}

bool ConnectorCOM::readReply_vector( quint32 timeout )
{
    bool start_received = false;
    quint16 reply_length = 0;
    reply_vector.clear();

    logger->addErrorLog("Начинаем прием данных от кассы");

    while( serial.waitForReadyRead( timeout ) ) // 5000
    {
        //logger->addErrorLog("Прием данных");
        QByteArray qqq = serial.readAll();

        if( !start_received )
        {
            for( int i=0; i<qqq.size(); i++ )
            {
                if( qqq[i] == 0x06 )
                {
                    logger->addErrorLog("Касса занята");
                }
                else if( qqq[i] == 0x02 )
                {
                    logger->addErrorLog( "Получен код начала данных от кассы" );
                    start_received = true;

                    //qDebug() << "Before: " << qqq.toHex();

                    qqq = QByteArray( qqq.data()+i, qqq.size()-i );
                    //qDebug() << " After: " << qqq.toHex();
                    break;
                }
            }
        }

        if( !start_received )
        {
            logger->addErrorLog( QString("Непонятные данные от кассы [%1]").arg( QString(qqq.toHex())) );
            continue;
        }

        reply_vector.append( qqq );
        logger->addErrorLog( QString("Кусок данных [%1]").arg( QString(qqq.toHex())) );

        if( reply_length == 0 && reply_vector.length() >= 3 ) // length received
        {
            reply_length = (quint16)( ((unsigned char)reply_vector[2]) << 8 | ((unsigned char)reply_vector[1]) ) + 3;
            logger->addErrorLog( QString("COM получили длину посылки [%1]").arg( reply_length ) );
        }

        // stop condition
        if( reply_length > 0 && ( reply_vector.length() == reply_length ) )
        {
            logger->addErrorLog( QString("COM Условие остановки приема, длина данных %1").arg(reply_length) );            
            logger->addErrorLog( QString( "COM Reply[%1]: %2" ).arg(reply_vector.size()).arg( QString(reply_vector.toHex()) ) );
            return true;
        }
    }

    logger->addErrorLog( QString("COM Ошибка в получении данных от кассы, принято [%1] байт").arg(reply_vector.length()) );

    return false;
}


bool ConnectorCOM::init( QString port )
{
    serial.setPortName( port );    

    if( !serial.open( QIODevice::ReadWrite ) )
    {        
        logger->addErrorLog( QString("Ошибка запуска Меркурий-119F на порту %1").arg( port ) );
        return false;
    }

    serial.setBaudRate( QSerialPort::Baud115200 );
    serial.setDataBits( QSerialPort::Data8 );
    serial.setParity( QSerialPort::NoParity );
    serial.setStopBits( QSerialPort::OneStop );

    logger->addErrorLog( QString("COM порт %1 открыт нормально").arg( port ) );

    return true;
}
