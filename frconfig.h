#ifndef FRCONFIG_H
#define FRCONFIG_H

#include <QDialog>
#include <QSettings>
#include <QDebug>
#include <QMessageBox>
#include <QtSerialPort/QSerialPortInfo>
#include <QHostAddress>

#define KASSA_INI "winkassa.ini"

#define OFD_INT_MIN 5
#define OFD_INT_MAX 360

namespace Ui {
class FRConfig;
}

class FRConfig : public QDialog
{
    Q_OBJECT

public:
    explicit FRConfig(QWidget *parent = 0);
    ~FRConfig();

    quint32 getHttpPort() { return httpPort; }
    QString getInterface() { return interface; }
    QString getComPort() { return comPort; }
    QHostAddress getOFDServer() { return ofd_server; }
    quint32 getOFDPort() { return ofd_port; }
    quint32 getOFDInterval() { return ofd_interval; }

private:
    Ui::FRConfig *ui;

    QString interface;
    QString comPort;
    QHostAddress ofd_server;

    bool loadSettings();
    bool saveSettings();
    void closeEvent( QCloseEvent *event );

    quint32 httpPort;
    quint32 ofd_port;
    quint32 ofd_interval;

signals:
    void configComplete();

private slots:
    void on_button_saveconfig_clicked();
};

#endif // FRCONFIG_H
