#-------------------------------------------------
#
# Project created by QtCreator 2017-02-14T13:15:46
#
#-------------------------------------------------

QT       += core gui network serialport
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Win-Kassa-119F
TEMPLATE = app
CONFIG += c++11

RC_FILE = winkassa.rc

win32 {
    LIBS += -lusb0
} else {
   LIBS += -lusb
}

SOURCES += main.cpp\
        mainwindow.cpp \
    httpserver.cpp \
    protohandler.cpp \
    frconfig.cpp \
    mercury119f.cpp \
    frcommand119f.cpp \
    connectorcom.cpp \
    connectorusb.cpp \
    httpthread.cpp \
    operationdb.cpp \
    logger.cpp

HEADERS  += mainwindow.h \
    httpserver.h \
    protohandler.h \
    frconfig.h \
    mercury119f.h \
    frcommand119f.h \
    baseconnector.h \
    connectorcom.h \
    connectorusb.h \
    httpthread.h \
    operationdb.h \
    logger.h

FORMS    += mainwindow.ui \
    frconfig.ui

DISTFILES += \
    icons/kassa100.png

RESOURCES += \
    winkassares.qrc
