#include "frconfig.h"
#include "ui_frconfig.h"

FRConfig::FRConfig(QWidget *parent) : QDialog(parent), ui(new Ui::FRConfig)
{
    ui->setupUi(this);

    if( !loadSettings() )
        this->show();
}

FRConfig::~FRConfig()
{
    delete ui;
}

void FRConfig::closeEvent( QCloseEvent *event )
{
    emit configComplete();
    QWidget::closeEvent(event);
}

bool FRConfig::loadSettings()
{
    bool configured = true;
    QSettings settings(KASSA_INI, QSettings::IniFormat);

    settings.beginGroup("settings");

    httpPort = settings.value("http_port").toUInt();
    ofd_port = settings.value("ofd_port").toUInt();
    ofd_interval = settings.value("ofd_interval").toUInt();

    if( httpPort == 0 )
        httpPort = 1000;

    if( ofd_port > 0 )
        ui->ofd_port->setValue( ofd_port );

    if( ofd_interval < OFD_INT_MIN || ofd_interval > OFD_INT_MAX )
        ofd_interval = 60;

    ui->spinBox_2->setValue( ofd_interval );
    ui->spinBox->setValue( httpPort );

    interface = settings.value( "interface" ).toString();

    if( interface == "USB" )
        ui->radioUSB->setChecked( true );
    else if( interface == "COM" )
        ui->radioCOM->setChecked( true );
    else
        configured = false;

    if( ofd_server.setAddress( settings.value("ofd_server").toString() ) == false )
        configured = false;
    else
        ui->ofd_server->setText( ofd_server.toString() );

    comPort = settings.value( "com_port" ).toString();

    if( comPort != "" )
        ui->comboKassaPort->addItem( comPort );    

    foreach( const QSerialPortInfo &info, QSerialPortInfo::availablePorts() )
    {
        if( info.portName() != comPort )
            ui->comboKassaPort->addItem( info.portName() );
    }

    settings.endGroup();

    return configured;
}

bool FRConfig::saveSettings()
{
    QSettings settings(KASSA_INI, QSettings::IniFormat);

    settings.beginGroup("settings");

    httpPort = ui->spinBox->value();
    ofd_port = ui->ofd_port->value();
    ofd_interval = ui->spinBox_2->value();

    QString ofd_server_string = ui->ofd_server->text();

    if( ofd_interval < OFD_INT_MIN )
        ofd_interval = OFD_INT_MIN;
    else if( ofd_interval > OFD_INT_MAX )
        ofd_interval = OFD_INT_MAX;

    settings.setValue( "ofd_interval", ofd_interval );

    if( ofd_port == 0 )
        return false;
    else
        settings.setValue( "ofd_port", ofd_port );

    if( ofd_server.setAddress( ofd_server_string ) )
        settings.setValue( "ofd_server", ofd_server_string );
    else
        return false;

    settings.setValue( "http_port", httpPort );        

    if( ui->radioUSB->isChecked() )
    {
        interface = "USB";
    }
    else if( ui->radioCOM->isChecked() )
    {
        interface = "COM";
        comPort = ui->comboKassaPort->currentText();
    }
    else
    {
        return false;
    }

    settings.setValue( "interface", interface );
    settings.setValue( "com_port", comPort );

    settings.endGroup();

    return true;
}

void FRConfig::on_button_saveconfig_clicked()
{
    if( saveSettings() )
    {
        this->hide();
        emit configComplete();
    }
    else
        QMessageBox::information( this, "Achtung!", "Неправильная конфигурация", 0 );
}
