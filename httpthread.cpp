#include "httpthread.h"

HttpThread::HttpThread( int socketDescriptor, ProtoHandler *ph ) :
    socketDescriptor(socketDescriptor),
    phandler(ph)
{

}

void HttpThread::run()
{
    QTcpSocket tcpSocket;

    if( !tcpSocket.setSocketDescriptor(socketDescriptor) )
    {
        emit error( tcpSocket.error() );
        return;
    }

    processConnection( &tcpSocket );
}

void HttpThread::processConnection( QTcpSocket *socket )
{
    QByteArray inData;

    while( socket->waitForReadyRead( 100 ) )
           inData += socket->readAll();

    QString rep = parseHttpRequest( inData );

    if( rep.length() > 0 )
    {
        socket->write("HTTP/1.0 200 OK\r\n");
        QString cl = QString("Content-Length: %1\r\n").arg( rep.length() );
        socket->write( cl.toLatin1() );
        socket->write("Content-Type: application/json\r\n");
        socket->write("Access-Control-Allow-Origin: *\r\n");

        socket->write("\r\n");
        socket->write( rep.toLatin1() );
    }
    else
    {
        socket->write("HTTP/1.0 400 Bad Request");
    }

    socket->flush();
    socket->waitForBytesWritten(3000);
    socket->close();
}

QString HttpThread::parseHttpRequest( QByteArray in )
{
    QString http_req( QByteArray( in, in.indexOf( "\r\n\r\n", 4 ) ) );
    QString json_data( in.begin()+in.indexOf( "\r\n\r\n", 4 )+4 );

    QString uri;
    QStringList http_req_list = http_req.split("\r\n");
    QStringList http_header = http_req_list.at(0).split(" ");

    QString ret;

    if( http_header.at(0) == "POST" )
    {
        uri = http_header.at( 1 );

        emit message( QString("Запрос %1").arg( uri ) );
        ret = phandler->parseRequest( uri, json_data );
        emit message( QString("Ответ %1").arg( ret ) );
    }
    else
    {
        ret = QString("This is Win-Kassa HTTP server, but you are using it wrong!");
    }

    return ret;
}

