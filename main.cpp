#include "mainwindow.h"
#include <QApplication>
#include "logger.h"

Logger *logger;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    logger = new Logger();

    if( w.createAllObjects() )
    {
        w.show();
        a.exec();
    }

    return 0;
}
