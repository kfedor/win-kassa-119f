#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>

#include "protohandler.h"
#include "httpthread.h"

class HttpServer : public QObject//: public QTcpServer
{
    Q_OBJECT

public:
    explicit HttpServer( ProtoHandler *ptr = Q_NULLPTR );
    bool start( quint32 port );
    void stop();

signals:
    void message(QString msg);

public slots:
    void newConnection();

//protected:
   // void incomingConnection(qintptr socketDescriptor) override;

private:
    QTcpServer *server;
    ProtoHandler *phandler;
    QString parseHttpRequest( QByteArray in );
    //static void processConnection( QTcpSocket *socket, HttpServer *http );
};

#endif // HTTPSERVER_H


