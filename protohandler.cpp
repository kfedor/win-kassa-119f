#include "protohandler.h"

extern Logger *logger;

ProtoHandler::ProtoHandler( Mercury119F *m, QHostAddress os, quint32 op, quint32 ofd_int ) : mercury( m ), ofd_server(os), ofd_port(op)
{    
    timer = new QTimer();
    connect( timer, SIGNAL( timeout() ), this, SLOT( timerTick() ) );
    //timer->start( 5000 );
    timer->start( 1000 * ofd_int );

    socketOFD = new QTcpSocket();

    connect( socketOFD, SIGNAL( connected() ), this, SLOT( OFDConnected() ) );
    connect( socketOFD, SIGNAL( readyRead() ), this, SLOT( OFDReply() ) );
    //connect( socketOFD, &QTcpSocket::connected, this, OFDConnected );
    //connect( socketOFD, &QIODevice::readyRead, this, OFDReply );
    connect( socketOFD, SIGNAL( error(QAbstractSocket::SocketError) ), this, SLOT( OFDError(QAbstractSocket::SocketError) ), Qt::DirectConnection );
}

ProtoHandler::~ProtoHandler()
{
    timer->stop();
    delete socketOFD;
}

void ProtoHandler::OFDError( QAbstractSocket::SocketError error )
{
    logger->addErrorLog( QString( "Ошибка соединения с ОФД: %1").arg( socketOFD->errorString() ) );
    socketOFD->abort();
    socketOFD->close();
}

void ProtoHandler::timerTick()
{
    if( mercury == Q_NULLPTR )
        return;

    QMutexLocker locker( &mutex );

    if( !mercury->readMessageForOFDServer() )
    {
        socketOFD->close();
        return;
    }

    ofd_data = mercury->getOFDMessage();

    if( ofd_data.size() > 0 )
    {
        socketOFD->abort();
        socketOFD->connectToHost( ofd_server, ofd_port );
    }
}

void ProtoHandler::OFDConnected()
{
    logger->addErrorLog( QString("Отправка ОФД сообщения для сервера %1 байт").arg( ofd_data.size() ) );

    if( socketOFD->write( ofd_data.data(), ofd_data.size() ) == -1 )
    {
        logger->addErrorLog( "Ошибка записи в сокет для ОФД сервера" );
        socketOFD->close();
    }

    ofd_data.clear();
}

void ProtoHandler::OFDReply()
{
    QMutexLocker locker( &mutex );

    QByteArray reply;
    reply += socketOFD->readAll();

    logger->addErrorLog( QString("Получен ответ от ОФД сервера %1 байт").arg( reply.size()) );

    mercury->writeMessageFromOFDServer( reply );
    socketOFD->close();    
}

QString ProtoHandler::parseRequest( QString uri, QString request )
{    
    QJsonObject root;
    QJsonObject value;

    QMutexLocker locker(&mutex);

    logger->addErrorLog( "HTTP Request: " + request );   

    if( !parseJson( request, &root ) )
    {
        value["error_string"] = "Parse error";
        value["error_code"] = 100;
    }
    else if( uri == "/v2/print" )
    {
        value = parse_v2_print( root );
    }
    else if( uri == "/v2/datetime" )
    {
        value = parse_v2_datetime( root );
    }
    else if( uri == "/v2/openday" )
    {
        value = parse_v2_openday( root );
    }
    else if( uri == "/v2/closeday" )
    {
        value = parse_v2_closeday( root );
    }
    else if( uri == "/v2/internal_report" )
    {
        value = parse_v2_internal_report( root );
    }
    else if( uri == "/v2/x_report" )
    {
        value = parse_v2_x_report( root );
    }
    else if( uri == "/v2/fiscal_document" )
    {
        value = parse_v2_fiscal_document( root );
    }
    else
    {
        value["error_string"] = "Unknown command";
        value["error_code"] = PROTO_ERR_UNKNOWN_COMMAND;
    }

    value["version"] = KASSA_VERSION;

    QJsonDocument doc( value );
    QByteArray data = doc.toJson( QJsonDocument::Compact );

    return QString::fromStdString( data.data() );
}

bool ProtoHandler::parseJson( QString jstring, QJsonObject *jobj )
{
    QJsonDocument root = QJsonDocument::fromJson( jstring.toUtf8() );    

    if( !root.isNull() && root.isObject() )
    {
        *jobj = root.object();
    }
    else
    {    
        return false;
    }

    return true;
}

QByteArray ProtoHandler::utf8_866( QString in )
{
    QTextCodec *codec = QTextCodec::codecForName("IBM 866");
    QByteArray encodedString = codec->fromUnicode( in );

    return encodedString;

    //return QString( encodedString );
}

QVector<QByteArray> ProtoHandler::getStringArray( QJsonArray data )
{
    QVector<QByteArray> strings866;

    for( auto i: data )
    {
        if( i.isString() )
        {
            QByteArray tmp =  utf8_866( i.toString() );

            if( tmp.length() > 40 )
                throw ProtoException( 103, "exceeded max string length(40)" );

            strings866.push_back( tmp );
        }
    }        

    return strings866;
}

// ------------------------------------------------------------------------------------------
QJsonObject ProtoHandler::parse_v2_print( QJsonObject root )
{
    QJsonObject val;
    QVector<QByteArray> strings866;
    QJsonArray jsonStrings;

    try
    {
        if( !root["request"].isObject() )
            throw ProtoException( PROTO_ERR_NO_REQUEST, "missing request" );

        QJsonObject request = root["request"].toObject();

        if( request["strings"].isArray() && request["strings"].toArray().size() > 0 )
        {
            jsonStrings = request["strings"].toArray();
            strings866 = getStringArray( jsonStrings );
        }

        QByteArray data;
        for( auto i: strings866 )
        {
            data += i;
            data += 0x0A;
        }

        data += QByteArray( 5, 0x0A ); // new line
        data += 0x1B; // cut paper
        data += 0x64;
        data += 0x30;
        data += 0x0C; // print buffer

        if( mercury->writeToFR( data ) == false )
            throw ProtoException( PROTO_ERR_EXEC, "error executing fr command" );

        QJsonObject response;
        response["result_code"] = mercury->getCmdResult();

        val["response"] = response;
    }

    catch( ProtoException &ex )
    {
        val["error_code"] = ex.getErrorCode();
        val["error_string"] = ex.what();
        return val;
    }

    val["error_code"] = PROTO_ERR_NOERR;
    return val;
}


QJsonObject ProtoHandler::parse_v2_datetime( QJsonObject root )
{
    QJsonObject val;

    try
    {
        if( !root["request"].isObject() )
            throw ProtoException( PROTO_ERR_NO_REQUEST, "missing request" );

        QJsonObject request = root["request"].toObject();

        QString date = request["date"].toString();
        QString time = request["time"].toString();        

        if( date.length() != 8 )
            throw ProtoException( PROTO_ERR_WRONGDATE, "Bad date" );

        if( time.length() != 4 )
            throw ProtoException( PROTO_ERR_WRONGTIME, "Bad time" );

        QDate qdate = QDate::fromString( date, "ddMMyyyy" );
        QTime qtime = QTime::fromString( time, "HHmm" );        
        QDateTime reqDateTime = QDateTime( qdate, qtime, Qt::UTC );
        quint32 unixtime = reqDateTime.toTime_t();

        if( mercury->setDateTime( unixtime ) == false )
               throw ProtoException( PROTO_ERR_EXEC, "error executing fr command" );

        QJsonObject response;
        quint16 cmd_result = mercury->getCmdResult();
        response["result_code"] = cmd_result;
        //response["result_string"] = mercury->cmdResultToString( cmd_result );

        val["response"] = response;
    }

    catch( ProtoException &ex )
    {
        val["error_code"] = ex.getErrorCode();
        val["error_string"] = ex.what();
        return val;
    }

    val["error_code"] = PROTO_ERR_NOERR;
    return val;

}

QJsonObject ProtoHandler::parse_v2_openday( QJsonObject root )
{
    QJsonObject val;

    try
    {
        if( !root["request"].isObject() )
            throw ProtoException( PROTO_ERR_NO_REQUEST, "missing request" );

        QJsonObject request = root["request"].toObject();
        QString name = request["cashier_name"].toString();

        if( name.length() == 0 )
            throw ProtoException( PROTO_ERR_CASHIER_EMPTY, "invalid cashier name(empty)" );

        QByteArray name866 = utf8_866( name );

        if( name866.length() > 64 )
            throw ProtoException( PROTO_ERR_CASHIER_EXCEED, "exceeded cashier name length(64)" );

        if( mercury->registerCashier( name866 ) == false )
            throw ProtoException( PROTO_ERR_EXEC, "error executing fr command" );

        QJsonObject response;
        quint16 cmd_result = mercury->getCmdResult();
        response["result_code"] = cmd_result;
        //response["result_string"] = mercury->cmdResultToString( cmd_result );

        val["response"] = response;
    }

    catch( ProtoException &ex )
    {
        val["error_code"] = ex.getErrorCode();
        val["error_string"] = ex.what();
        return val;
    }

    val["error_code"] = PROTO_ERR_NOERR;
    return val;
}

QJsonObject ProtoHandler::parse_v2_closeday( QJsonObject root )
{
    QJsonObject val;

    try
    {
        if( !root["request"].isObject() )
            throw ProtoException( PROTO_ERR_NO_REQUEST, "missing request" );

        QJsonObject request = root["request"].toObject();

        QString name = request["cashier_name"].toString();

        if( name.length() == 0 )
            throw ProtoException( PROTO_ERR_CASHIER_EMPTY, "invalid cashier name(empty)" );

        QByteArray name866 = utf8_866( name );

        if( name866.length() > 64 )
            throw ProtoException( PROTO_ERR_CASHIER_EXCEED, "exceeded cashier name length(64)" );

        if( mercury->ZReport( name866 ) == false )
            throw ProtoException( PROTO_ERR_EXEC, "error executing fr command" );

        QJsonObject response;
        quint16 cmd_result = mercury->getCmdResult();
        response["result_code"] = cmd_result;
        //response["result_string"] = mercury->cmdResultToString( cmd_result );

        val["response"] = response;

        if( cmd_result == 0 ) // нормально закрыли смену
            operDB.newOperationDay();
    }

    catch( ProtoException &ex )
    {
        val["error_code"] = ex.getErrorCode();
        val["error_string"] = ex.what();
        return val;
    }

    val["error_code"] = PROTO_ERR_NOERR;
    return val;
}

QJsonObject ProtoHandler::parse_v2_internal_report( QJsonObject root )
{
    QJsonObject val;

    try
    {
        QJsonArray last_arr;
        QByteArray data = operDB.getLastOperDayForPrint( last_arr );
        /*
         * v2.23 возвращаем печать сводного отчета
        */
        // Print сводный отчет
        if( mercury->writeToFR( data ) == false )
            throw ProtoException( PROTO_ERR_EXEC, "error executing fr command" );

        QJsonObject response;
        quint16 cmd_result = mercury->getCmdResult();

        response["result_code"] = cmd_result;

        val["response"] = response;

        val["data"] = last_arr;
    }

    catch( ProtoException &ex )
    {
        val["error_code"] = ex.getErrorCode();
        val["error_string"] = ex.what();
        return val;
    }

    val["error_code"] = PROTO_ERR_NOERR;
    return val;
}

QJsonObject ProtoHandler::parse_v2_x_report( QJsonObject root )
{
    QJsonObject val;

    try
    {
        if( mercury->XReport() == false )
            throw ProtoException( PROTO_ERR_EXEC, "error executing fr command" );

        QJsonObject response;
        quint16 cmd_result = mercury->getCmdResult();
        response["result_code"] = cmd_result;

        val["response"] = response;
    }

    catch( ProtoException &ex )
    {
        val["error_code"] = ex.getErrorCode();
        val["error_string"] = ex.what();
        return val;
    }

    val["error_code"] = PROTO_ERR_NOERR;
    return val;
}


QJsonObject ProtoHandler::parse_v2_fiscal_document( QJsonObject root )
{
    QJsonObject val;

    try
    {
        if( !root["request"].isObject() )
            throw ProtoException( PROTO_ERR_NO_REQUEST, "missing request" );

        QJsonObject request = root["request"].toObject();

        QString amount_type = request["amount_type"].toString();
        QString type = request["type"].toString();
        QString cashier_name = request["cashier_name"].toString();
        QString product_name = request["product_name"].toString();       
        QString amount = request["amount"].toString();

        if( cashier_name.length() == 0 )
            throw ProtoException( PROTO_ERR_CASHIER_EMPTY, "invalid cashier name(empty)" );

        if( product_name.length() == 0 )
            throw ProtoException( PROTO_ERR_NO_PRODUCT, "invalid product name(empty)" );

        if( amount.length() < 4 ) // 0.01
            throw ProtoException( PROTO_ERR_NO_AMOUNT, "invalid amount(empty)" );

        QByteArray cashier_name866 = utf8_866( cashier_name );
        QByteArray product_name866 = utf8_866( product_name );

        if( cashier_name866.length() > 64 )
            throw ProtoException( PROTO_ERR_CASHIER_EXCEED, "exceeded cashier_name length(64)" );

        if( product_name866.length() > 64 )
            throw ProtoException( PROTO_ERR_PRODUCT_EXCEED, "exceeded product_name length(64)" );

        Mercury119F::doc_attr doc_attr;

        if( type == S_SALE )
            doc_attr = Mercury119F::doc_attr::sale;
        else if( type == S_WITHDRAWAL )
            doc_attr = Mercury119F::doc_attr::withdrawal;
        else if( type == S_SALE_RETURN )
            doc_attr = Mercury119F::doc_attr::sale_return;
        else if( type == S_WITH_RETURN )
            doc_attr = Mercury119F::doc_attr::withdrawal_return;
        else
            throw ProtoException( PROTO_ERR_BAD_FISCAL_TYPE, "Unsupported fiscal document type" );

        Mercury119F::amount_type atype;

        if( amount_type == S_CASH )
            atype = Mercury119F::amount_type::amount_cash;
        else if( amount_type == S_CARD )
            atype = Mercury119F::amount_type::amount_card;
        else
            throw ProtoException( PROTO_ERR_BAD_OPER, "Unsupported amount type" );        

        bool double_ok;
        quint32 amount_int = (quint32)round( amount.toDouble( &double_ok ) * 100 );

        if( double_ok == false )
            throw ProtoException( PROTO_ERR_NO_AMOUNT, "invalid amount(not double)" );

        if( mercury->sale( atype, amount_int, amount_int, product_name866, cashier_name866, doc_attr ) == false )
            throw ProtoException( PROTO_ERR_EXEC, "error executing fr command" );

        QJsonObject response;
        quint16 cmd_result = mercury->getCmdResult();
        response["result_code"] = cmd_result;
        //response["result_string"] = mercury->cmdResultToString( cmd_result );

        val["response"] = response;

        // Запишем в файл операцию
        if( cmd_result == 0 )
            operDB.addOperation( amount_type, type, amount, amount, product_name );
    }

    catch( ProtoException &ex )
    {
        val["error_code"] = ex.getErrorCode();
        val["error_string"] = ex.what();
        return val;
    }

    val["error_code"] = PROTO_ERR_NOERR;
    return val;
}
