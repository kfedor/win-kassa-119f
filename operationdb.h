#ifndef OPERATIONDB_H
#define OPERATIONDB_H

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QFile>
#include <QDebug>
#include <QTextCodec>
#include <QDateTime>

#include <logger.h>

#define OPERATION_FILE "oper_db.json"

#define S_SALE "sale"
#define S_WITHDRAWAL "withdrawal"
#define S_SALE_RETURN "sale_return"
#define S_WITH_RETURN "withdrawal_return"

#define S_CASH "cash"
#define S_CARD "card"

#define TIME_FORMAT "dd.MM.yyyy hh:mm:ss"

class OperationDB
{
public:
    OperationDB();
    bool addOperation( QString, QString, QString, QString, QString );
    bool newOperationDay();
    QByteArray getLastOperDayForPrint( QJsonArray & );

private:
    QJsonDocument loadJson();
    bool saveJson( QJsonDocument );
    QByteArray utf8_866( QString in );
};

#endif // OPERATIONDB_H
