#ifndef FRCOMMAND119F_H
#define FRCOMMAND119F_H

#include <QByteArray>

#define P_CCITT  0x1021

class FRCommand119F
{
public:    
    FRCommand119F()
    {
        initCrcCcittTab();
    }

    void addRequisiteASCII( quint16 req_id, QByteArray data );
    void addRequisiteVLN( quint16 req_id, quint32 value );
    void addRequisiteFVLN( quint16 req_id, quint16 value );
    void addRequisiteByte( quint16 req_id, char value );
    void addByteArray( QByteArray in );
    void addChar( unsigned char );
    void addInt16( quint16 );
    void addInt32( quint32 );
    void addCRCCcitt(); // For 119F
    quint16 getCRCCcitt( QByteArray );

    void print();
    QByteArray get() { return cmd; }
    quint16 size() { return cmd.size(); }

private:
    QByteArray cmd;

    quint16 crc_tabccitt[256];
    void initCrcCcittTab( void );
    quint16 updateCrcCcitt( quint16 crc, char c );
};

#endif // FRCOMMAND119F_H
